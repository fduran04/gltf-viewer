#include "ViewerApplication.hpp"

#include <iostream>
#include <numeric>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/io.hpp>

#include "utils/cameras.hpp"
#include "utils/gltf.hpp"
#include "utils/images.hpp"

#include <stb_image_write.h>

void keyCallback(
    GLFWwindow *window, int key, int scancode, int action, int mods)
{
  if (key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE) {
    glfwSetWindowShouldClose(window, 1);
  }
}

bool ViewerApplication::loadGltfFile(tinygltf::Model &model)
{
  tinygltf::TinyGLTF loader;
  std::string err;
  std::string warn;

  bool ret =
      loader.LoadASCIIFromFile(&model, &err, &warn, m_gltfFilePath.string());

  if (!warn.empty()) {
    printf("Warn: %s\n", warn.c_str());
  }

  if (!err.empty()) {
    printf("Err: %s\n", err.c_str());
  }

  if (!ret) {
    printf("Failed to parse glTF\n");
    return -1;
  }

  return ret;
}

std::vector<GLuint> ViewerApplication::computeTangents(
    const tinygltf::Model &model, const tinygltf::Mesh &mesh)
{
  std::vector<GLuint> buffers;
  std::vector<float> tangents;
  for (size_t pIdx = 0; pIdx < mesh.primitives.size(); ++pIdx) {
    tangents.clear();
    const auto &primitive = mesh.primitives[pIdx];
    const auto positionAttrIdxIt = primitive.attributes.find("POSITION");
    if (positionAttrIdxIt == end(primitive.attributes)) {
      continue;
    }
    const auto &positionAccessor = model.accessors[(*positionAttrIdxIt).second];
    if (positionAccessor.type != 3) {
      std::cerr << "Position accessor with type != VEC3, skipping" << std::endl;
      continue;
    }
    const auto &positionBufferView =
        model.bufferViews[positionAccessor.bufferView];
    const auto positionByteOffset =
        positionAccessor.byteOffset + positionBufferView.byteOffset;
    const auto &positionBuffer = model.buffers[positionBufferView.buffer];
    const auto positionByteStride = positionBufferView.byteStride
                                        ? positionBufferView.byteStride
                                        : 3 * sizeof(float);

    const auto textureAttrIdxIt = primitive.attributes.find("TEXCOORD_0");
    if (textureAttrIdxIt == end(primitive.attributes)) {
      continue;
    }
    const auto &textureAccessor = model.accessors[(*textureAttrIdxIt).second];
    if (textureAccessor.type != 2) {
      std::cerr << "Texture accessor with type != VEC2, skipping" << std::endl;
      continue;
    }
    const auto &textureBufferView =
        model.bufferViews[textureAccessor.bufferView];
    const auto textureByteOffset =
        textureAccessor.byteOffset + textureBufferView.byteOffset;
    const auto &textureBuffer = model.buffers[textureBufferView.buffer];
    const auto textureByteStride = textureBufferView.byteStride
                                       ? textureBufferView.byteStride
                                       : 2 * sizeof(float);

    if (primitive.indices >= 0) {
      const auto &indexAccessor = model.accessors[primitive.indices];
      const auto &indexBufferView = model.bufferViews[indexAccessor.bufferView];
      const auto indexByteOffset =
          indexAccessor.byteOffset + indexBufferView.byteOffset;
      const auto &indexBuffer = model.buffers[indexBufferView.buffer];
      auto indexByteStride = indexBufferView.byteStride;

      switch (indexAccessor.componentType) {
      default:
        std::cerr << "Primitive index accessor with bad componentType "
                  << indexAccessor.componentType << ", skipping it."
                  << std::endl;
        continue;
      case TINYGLTF_COMPONENT_TYPE_UNSIGNED_BYTE:
        indexByteStride = indexByteStride ? indexByteStride : sizeof(uint8_t);
        break;
      case TINYGLTF_COMPONENT_TYPE_UNSIGNED_SHORT:
        indexByteStride = indexByteStride ? indexByteStride : sizeof(uint16_t);
        break;
      case TINYGLTF_COMPONENT_TYPE_UNSIGNED_INT:
        indexByteStride = indexByteStride ? indexByteStride : sizeof(uint32_t);
        break;
      }

      for (size_t i = 0; i < indexAccessor.count; i += 3) {
        uint32_t index1 = 0;
        uint32_t index2 = 0;
        uint32_t index3 = 0;
        switch (indexAccessor.componentType) {
        case TINYGLTF_COMPONENT_TYPE_UNSIGNED_BYTE:
          index1 = *((const uint8_t *)&indexBuffer
                         .data[indexByteOffset + indexByteStride * i]);
          index2 = *((const uint8_t *)&indexBuffer
                         .data[indexByteOffset + indexByteStride * (i + 1)]);
          index3 = *((const uint8_t *)&indexBuffer
                         .data[indexByteOffset + indexByteStride * (i + 2)]);
          break;
        case TINYGLTF_COMPONENT_TYPE_UNSIGNED_SHORT:
          index1 = *((const uint16_t *)&indexBuffer
                         .data[indexByteOffset + indexByteStride * i]);
          index2 = *((const uint16_t *)&indexBuffer
                         .data[indexByteOffset + indexByteStride * (i + 1)]);
          index3 = *((const uint16_t *)&indexBuffer
                         .data[indexByteOffset + indexByteStride * (i + 2)]);
          break;
        case TINYGLTF_COMPONENT_TYPE_UNSIGNED_INT:
          index1 = *((const uint32_t *)&indexBuffer
                         .data[indexByteOffset + indexByteStride * i]);
          index2 = *((const uint32_t *)&indexBuffer
                         .data[indexByteOffset + indexByteStride * (i + 1)]);
          index3 = *((const uint32_t *)&indexBuffer
                         .data[indexByteOffset + indexByteStride * (i + 2)]);
          break;
        }

        const auto &v0 =
            *((const glm::vec3 *)&positionBuffer
                    .data[positionByteOffset + positionByteStride * index1]);
        const auto &v1 =
            *((const glm::vec3 *)&positionBuffer
                    .data[positionByteOffset + positionByteStride * index2]);
        const auto &v2 =
            *((const glm::vec3 *)&positionBuffer
                    .data[positionByteOffset + positionByteStride * index3]);
        const auto &uv0 =
            *((const glm::vec2 *)&textureBuffer
                    .data[textureByteOffset + textureByteStride * index1]);
        const auto &uv1 =
            *((const glm::vec2 *)&textureBuffer
                    .data[textureByteOffset + textureByteStride * index2]);
        const auto &uv2 =
            *((const glm::vec2 *)&textureBuffer
                    .data[textureByteOffset + textureByteStride * index3]);
        glm::vec3 tangent = computeTangent(v0, v1, v2, uv0, uv1, uv2);
        for (int i = 0; i < 3; i++) {
          for (int j = 0; j < 3; j++) {
            tangents.emplace_back(tangent[j]);
          }
        }
      }
    } else {
      for (size_t i = 0; i < positionAccessor.count; i += 3) {
        const auto &v0 =
            *((const glm::vec3 *)&positionBuffer
                    .data[positionByteOffset + positionByteStride * i]);
        const auto &v1 =
            *((const glm::vec3 *)&positionBuffer
                    .data[positionByteOffset + positionByteStride * (i + 1)]);
        const auto &v2 =
            *((const glm::vec3 *)&positionBuffer
                    .data[positionByteOffset + positionByteStride * (i + 2)]);
        const auto &uv0 =
            *((const glm::vec2 *)&textureBuffer
                    .data[textureByteOffset + textureByteStride * i]);
        const auto &uv1 =
            *((const glm::vec2 *)&textureBuffer
                    .data[textureByteOffset + textureByteStride * (i + 1)]);
        const auto &uv2 =
            *((const glm::vec2 *)&textureBuffer
                    .data[textureByteOffset + textureByteStride * (i + 2)]);
        glm::vec3 tangent = computeTangent(v0, v1, v2, uv0, uv1, uv2);
        for (int i = 0; i < 3; i++) {
          for (int j = 0; j < 3; j++) {
            tangents.emplace_back(tangent[j]);
          }
        }
      }
    }
    GLuint buffer;
    glGenBuffers(1, &buffer);
    glBindBuffer(GL_ARRAY_BUFFER, buffer);
    glBufferStorage(
        GL_ARRAY_BUFFER, tangents.size() * sizeof(float), tangents.data(), 0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    buffers.emplace_back(buffer);
  }
  return buffers;
}

glm::vec3 ViewerApplication::computeTangent(const glm::vec3 &v0,
    const glm::vec3 &v1, const glm::vec3 &v2, const glm::vec2 &uv0,
    const glm::vec2 &uv1, const glm::vec2 &uv2)
{
  glm::vec3 deltaPos1 = v1 - v0;
  glm::vec3 deltaPos2 = v2 - v0;
  glm::vec2 deltaUV1 = uv1 - uv0;
  glm::vec2 deltaUV2 = uv2 - uv0;
  float r = 1.0f / (deltaUV1.x * deltaUV2.y - deltaUV1.y * deltaUV2.x);
  return (deltaPos1 * deltaUV2.y - deltaPos2 * deltaUV1.y) * r;
}

std::vector<GLuint> ViewerApplication::createBufferObjects(
    const tinygltf::Model &model)
{
  std::vector<GLuint> bufferObjects(
      model.buffers.size(), 0); // Assuming buffers is a std::vector of Buffer

  glGenBuffers(GLsizei(model.buffers.size()), bufferObjects.data());
  for (size_t i = 0; i < model.buffers.size(); ++i) {
    glBindBuffer(GL_ARRAY_BUFFER, bufferObjects[i]);
    glBufferStorage(GL_ARRAY_BUFFER,
        model.buffers[i].data.size(), // Assume a Buffer has a data member
                                      // variable of type std::vector
        model.buffers[i].data.data(), 0);
  }
  glBindBuffer(
      GL_ARRAY_BUFFER, 0); // Cleanup the binding point after the loop only
  return bufferObjects;
}

std::vector<GLuint> ViewerApplication::createVertexArrayObjects(
    const tinygltf::Model &model, const std::vector<GLuint> &bufferObjects,
    std::vector<VaoRange> &meshIndexToVaoRange)
{
  meshIndexToVaoRange.resize(model.meshes.size());

  std::vector<GLuint> vertexArrayObjects;

  GLuint VERTEX_ATTRIB_POSITION_IDX = 0;
  GLuint VERTEX_ATTRIB_NORMAL_IDX = 1;
  GLuint VERTEX_ATTRIB_TEXCOORD0_IDX = 2;
  GLuint VERTEX_ATTRIB_TANGENT_IDX = 3;

  for (size_t i = 0; i < model.meshes.size(); ++i) {

    const auto vaoOffset = vertexArrayObjects.size();

    const tinygltf::Mesh &mesh = model.meshes[i];

    std::vector<GLuint> tangentBuffers = computeTangents(model, mesh);

    vertexArrayObjects.resize(vaoOffset + mesh.primitives.size());
    meshIndexToVaoRange.push_back(
        VaoRange{(GLsizei)vaoOffset, (GLsizei)mesh.primitives.size()});
    glGenVertexArrays(
        mesh.primitives.size(), &vertexArrayObjects.data()[vaoOffset]);

    for (size_t j = 0; j < mesh.primitives.size(); ++j) {

      glBindVertexArray(vertexArrayObjects[vaoOffset + j]);
      const tinygltf::Primitive &primitive = mesh.primitives[j];
      {
        const auto iterator = primitive.attributes.find("POSITION");
        if (iterator != end(primitive.attributes)) {
          const auto accessorIdx = (*iterator).second;
          const auto &accessor = model.accessors[accessorIdx];
          const auto &bufferView = model.bufferViews[accessor.bufferView];
          const auto bufferIdx = bufferView.buffer;
          const auto bufferObject = bufferObjects[bufferIdx];
          glEnableVertexAttribArray(VERTEX_ATTRIB_POSITION_IDX);
          glBindBuffer(GL_ARRAY_BUFFER, bufferObject);
          const auto byteOffset = accessor.byteOffset + bufferView.byteOffset;
          glVertexAttribPointer(VERTEX_ATTRIB_POSITION_IDX, accessor.type,
              accessor.componentType, GL_FALSE, GLsizei(bufferView.byteStride),
              (const GLvoid *)byteOffset);
        }
      }

      {
        const auto iterator = primitive.attributes.find("NORMAL");
        if (iterator != end(primitive.attributes)) {
          const auto accessorIdx = (*iterator).second;
          const auto &accessor = model.accessors[accessorIdx];
          const auto &bufferView = model.bufferViews[accessor.bufferView];
          const auto bufferIdx = bufferView.buffer;
          const auto bufferObject = bufferObjects[bufferIdx];
          glEnableVertexAttribArray(VERTEX_ATTRIB_NORMAL_IDX);
          glBindBuffer(GL_ARRAY_BUFFER, bufferObject);
          const auto byteOffset = accessor.byteOffset + bufferView.byteOffset;
          glVertexAttribPointer(VERTEX_ATTRIB_NORMAL_IDX, accessor.type,
              accessor.componentType, GL_FALSE, GLsizei(bufferView.byteStride),
              (const GLvoid *)byteOffset);
        }
      }
      {
        const auto iterator = primitive.attributes.find("TEXCOORD_0");
        if (iterator != end(primitive.attributes)) {
          const auto accessorIdx = (*iterator).second;
          const auto &accessor = model.accessors[accessorIdx];
          const auto &bufferView = model.bufferViews[accessor.bufferView];
          const auto bufferIdx = bufferView.buffer;
          const auto bufferObject = bufferObjects[bufferIdx];
          glEnableVertexAttribArray(VERTEX_ATTRIB_TEXCOORD0_IDX);
          glBindBuffer(GL_ARRAY_BUFFER, bufferObject);
          const auto byteOffset = accessor.byteOffset + bufferView.byteOffset;
          glVertexAttribPointer(VERTEX_ATTRIB_TEXCOORD0_IDX, accessor.type,
              accessor.componentType, GL_FALSE, GLsizei(bufferView.byteStride),
              (const GLvoid *)byteOffset);
        }
      }
      {
        const auto iterator = primitive.attributes.find("TANGENT");
        if (iterator != end(primitive.attributes)) {
          const auto accessorIdx = (*iterator).second;
          const auto &accessor = model.accessors[accessorIdx];
          const auto &bufferView = model.bufferViews[accessor.bufferView];
          const auto bufferIdx = bufferView.buffer;
          const auto bufferObject = bufferObjects[bufferIdx];
          glEnableVertexAttribArray(VERTEX_ATTRIB_TANGENT_IDX);
          glBindBuffer(GL_ARRAY_BUFFER, bufferObject);
          const auto byteOffset = accessor.byteOffset + bufferView.byteOffset;
          glVertexAttribPointer(VERTEX_ATTRIB_TANGENT_IDX, accessor.type,
              accessor.componentType, GL_FALSE, GLsizei(bufferView.byteStride),
              (const GLvoid *)byteOffset);
        } else {
          const auto iterator = primitive.attributes.find("NORMAL");
          if (iterator != end(primitive.attributes)) {
            const auto accessorIdx = (*iterator).second;
            const auto &accessor = model.accessors[accessorIdx];
            glEnableVertexAttribArray(VERTEX_ATTRIB_TANGENT_IDX);
            glBindBuffer(GL_ARRAY_BUFFER, tangentBuffers[j]);
            glVertexAttribPointer(
                VERTEX_ATTRIB_TANGENT_IDX, 3, GL_FLOAT, GL_FALSE, 0, 0);
          }
        }
      }
      if (mesh.primitives[j].indices >= 0) {
        const auto accessorIdx = mesh.primitives[j].indices;
        const auto &accessor = model.accessors[accessorIdx];
        const auto &bufferView = model.bufferViews[accessor.bufferView];
        const auto bufferIdx = bufferView.buffer;
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bufferObjects[bufferIdx]);
      }
    }
  }
  glBindVertexArray(0);
  return vertexArrayObjects;
}

std::vector<GLuint> ViewerApplication::createTextureObjects(
    const tinygltf::Model &model) const
{
  std::vector<GLuint> textureObjects(model.textures.size());
  tinygltf::Sampler defaultSampler;
  defaultSampler.minFilter = GL_LINEAR;
  defaultSampler.magFilter = GL_LINEAR;
  defaultSampler.wrapS = GL_REPEAT;
  defaultSampler.wrapT = GL_REPEAT;
  defaultSampler.wrapR = GL_REPEAT;

  glActiveTexture(GL_TEXTURE0);

  glGenTextures(model.textures.size(), textureObjects.data());
  for (int i = 0; i < model.textures.size(); ++i) {

    const tinygltf::Texture &texture = model.textures[i];

    assert(texture.source >= 0); // ensure a source image is present
    const auto &image = model.images[texture.source]; // get the image
    glBindTexture(GL_TEXTURE_2D, textureObjects[i]);
    // fill the texture object with the data from the image
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image.width, image.height, 0,
        GL_RGBA, image.pixel_type, image.image.data());

    const auto &sampler =
        texture.sampler >= 0 ? model.samplers[texture.sampler] : defaultSampler;

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
        sampler.minFilter != -1 ? sampler.minFilter : GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,
        sampler.magFilter != -1 ? sampler.magFilter : GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, sampler.wrapS);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, sampler.wrapT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, sampler.wrapR);
    if (sampler.minFilter == GL_NEAREST_MIPMAP_NEAREST ||
        sampler.minFilter == GL_NEAREST_MIPMAP_LINEAR ||
        sampler.minFilter == GL_LINEAR_MIPMAP_NEAREST ||
        sampler.minFilter == GL_LINEAR_MIPMAP_LINEAR) {
      glGenerateMipmap(GL_TEXTURE_2D);
    }
  }

  glBindTexture(GL_TEXTURE_2D, 0);

  return textureObjects;
}

int ViewerApplication::run()
{
  // Loader shaders
  const auto glslDepthProgram =
      compileProgram({m_ShadersRootPath / m_vertexDepthShader,
          m_ShadersRootPath / m_fragmentDepthShader});

  const auto glslDebugDepthProgram =
      compileProgram({m_ShadersRootPath / m_vertexDebugDepthShader,
          m_ShadersRootPath / m_fragmentDebugDepthShader});

  const auto glslRenderingProgram =
      compileProgram({m_ShadersRootPath / m_vertexShader,
          m_ShadersRootPath / m_fragmentShader});

  tinygltf::Model model;
  // TODO Loading the glTF file
  if (!loadGltfFile(model)) {
    return -1;
  }

  glm::vec3 bbmin;
  glm::vec3 bbmax;
  // Build projection matrix
  computeSceneBounds(model, bbmin, bbmax);
  glm::vec3 diagonal = bbmax - bbmin;
  float distance = glm::length(diagonal);
  auto maxDistance =
      distance < 0.001
          ? 100.f
          : distance; // TODO use scene bounds instead to compute this
  maxDistance = maxDistance > 0.f ? maxDistance : 100.f;
  const auto projMatrix =
      glm::perspective(70.f, float(m_nWindowWidth) / m_nWindowHeight,
          0.001f * maxDistance, 1.5f * maxDistance);

  // TODO Implement a new CameraController model and use it instead. Propose
  // the choice from the GUI
  std::unique_ptr<CameraController> cameraController =
      std::make_unique<TrackballCameraController>(
          m_GLFWHandle.window(), 0.5f * maxDistance);
  if (m_hasUserCamera) {
    cameraController->setCamera(m_userCamera);
  } else {
    // TODO Use scene bounds to compute a better default camera

    glm::vec3 center = bbmin + diagonal * 0.5f;
    glm::vec3 eye;
    glm::vec3 up = glm::vec3(0, 1, 0);
    if (diagonal.z > 0) {
      eye = center + diagonal;
    } else {
      eye = center + 2.f * glm::cross(diagonal, up);
    }
    cameraController->setCamera(Camera{eye, center, up});
  }

  const auto sceneCenter = 0.5f * (bbmin + bbmax);
  const float sceneRadius = maxDistance * 0.5f;

  const auto lightProjMatrix = glm::ortho(-sceneRadius, sceneRadius,
      -sceneRadius, sceneRadius, 0.01f * sceneRadius, 2.f * sceneRadius);

  // TODO Creation of Buffer Objects
  const auto bufferObjects = createBufferObjects(model);
  std::vector<VaoRange> meshIndexToVaoRange;
  // TODO Creation of Vertex Array Objects
  const auto vertexArrayObjects =
      createVertexArrayObjects(model, bufferObjects, meshIndexToVaoRange);

  GLuint quadVBO, quadVAO;
  float quadVertices[] = {
      // positions        // texture Coords
      -1.0f,
      1.0f,
      0.0f,
      0.0f,
      1.0f,
      -1.0f,
      -1.0f,
      0.0f,
      0.0f,
      0.0f,
      1.0f,
      1.0f,
      0.0f,
      1.0f,
      1.0f,
      1.0f,
      -1.0f,
      0.0f,
      1.0f,
      0.0f,
  };
  // setup plane VAO
  glGenVertexArrays(1, &quadVAO);
  glGenBuffers(1, &quadVBO);
  glBindVertexArray(quadVAO);
  glBindBuffer(GL_ARRAY_BUFFER, quadVBO);
  glBufferData(
      GL_ARRAY_BUFFER, sizeof(quadVertices), &quadVertices, GL_STATIC_DRAW);
  glEnableVertexAttribArray(0);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void *)0);
  glEnableVertexAttribArray(1);
  glVertexAttribPointer(
      1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void *)(3 * sizeof(float)));

  const auto textureObjects = createTextureObjects(model);

  const unsigned int SHADOW_WIDTH = 1024, SHADOW_HEIGHT = 1024;

  GLuint depthMapFBO;
  glGenFramebuffers(1, &depthMapFBO);

  unsigned int depthMap;
  glGenTextures(1, &depthMap);
  glBindTexture(GL_TEXTURE_2D, depthMap);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, SHADOW_WIDTH,
      SHADOW_HEIGHT, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  glBindFramebuffer(GL_FRAMEBUFFER, depthMapFBO);
  glFramebufferTexture2D(
      GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depthMap, 0);
  glDrawBuffer(GL_NONE);
  glReadBuffer(GL_NONE);
  glBindFramebuffer(GL_FRAMEBUFFER, 0);

  GLuint whiteTexture;
  glGenTextures(1, &whiteTexture);
  glBindTexture(GL_TEXTURE_2D, whiteTexture);
  float white[4] = {1.f, 1.f, 1.f, 1.f};
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 1, 1, 0, GL_RGBA, GL_FLOAT, white);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, GL_REPEAT);
  glBindTexture(GL_TEXTURE_2D, 0);

  const auto uLightSpaceModelMatrixDepthLocation =
      glGetUniformLocation(glslDepthProgram.glId(), "uLightSpaceModelMatrix");

  const auto uDepthMapTextureLocationDebug =
      glGetUniformLocation(glslDebugDepthProgram.glId(), "uDepthMapTexture");

  const auto uLightSpaceModelMatrixLocation = glGetUniformLocation(
      glslRenderingProgram.glId(), "uLightSpaceModelMatrix");
  const auto uDepthMapTextureLocation =
      glGetUniformLocation(glslRenderingProgram.glId(), "uDepthMapTexture");
  const auto modelViewProjMatrixLocation =
      glGetUniformLocation(glslRenderingProgram.glId(), "uModelViewProjMatrix");
  const auto modelViewMatrixLocationRendering =
      glGetUniformLocation(glslRenderingProgram.glId(), "uModelViewMatrix");
  const auto normalMatrixLocation =
      glGetUniformLocation(glslRenderingProgram.glId(), "uNormalMatrix");
  const auto uLightDirectionLocation =
      glGetUniformLocation(glslRenderingProgram.glId(), "uLightDirection");
  const auto uLightIntensityLocation =
      glGetUniformLocation(glslRenderingProgram.glId(), "uLightIntensity");
  const auto uBaseColorTexture =
      glGetUniformLocation(glslRenderingProgram.glId(), "uBaseColorTexture");
  const auto uBaseColorFactor =
      glGetUniformLocation(glslRenderingProgram.glId(), "uBaseColorFactor");
  const auto uMetallicFactor =
      glGetUniformLocation(glslRenderingProgram.glId(), "uMetallicFactor");
  const auto uRougnessFactor =
      glGetUniformLocation(glslRenderingProgram.glId(), "uRougnessFactor");
  const auto uMetallicRoughnessTexture = glGetUniformLocation(
      glslRenderingProgram.glId(), "uMetallicRoughnessTexture");
  const auto uEmissiveTexture =
      glGetUniformLocation(glslRenderingProgram.glId(), "uEmissiveTexture");
  const auto uEmissiveFactor =
      glGetUniformLocation(glslRenderingProgram.glId(), "uEmissiveFactor");
  const auto uOcclusionTexture =
      glGetUniformLocation(glslRenderingProgram.glId(), "uOcclusionTexture");
  const auto uOcclusionStrength =
      glGetUniformLocation(glslRenderingProgram.glId(), "uOcclusionStrength");
  const auto uOcclusionOn =
      glGetUniformLocation(glslRenderingProgram.glId(), "uOcclusionOn");
  const auto uNormalMap =
      glGetUniformLocation(glslRenderingProgram.glId(), "uNormalMap");
  const auto uNormalMapOn =
      glGetUniformLocation(glslRenderingProgram.glId(), "uNormalMapOn");
  const auto uShadowMapOn =
      glGetUniformLocation(glslRenderingProgram.glId(), "uShadowMapOn");

  glm::vec3 lightDirection{1, 1, 1};
  glm::vec3 lightIntensity{1, 1, 1};

  static bool lightFromCamera = false;
  static bool occlusionOn = true;
  static bool normalMapOn = true;
  static bool debugDepth = false;
  static float theta = 0;
  static float phi = 0;
  static bool lightMoved = true;
  static bool shadowMappingOn = true;

  {
    float sinTheta = glm::sin(theta);
    lightDirection = {
        sinTheta * glm::cos(phi), glm::cos(theta), sinTheta * glm::sin(phi)};
  }

  const auto bindMaterial = [&](const auto materialIndex) {
    // only valid is materialIndex >= 0
    GLuint baseColorTextureObject = whiteTexture;
    GLuint metallicRoughnessTextureObject = 0;
    GLuint emissiveTextureObject = 0;
    GLuint occlusionTextureObject = whiteTexture;
    GLuint normalMapTextureObject = 0;

    std::vector<double> baseColorFactor(4, 1);
    if (materialIndex >= 0) {
      const tinygltf::Material &material = model.materials[materialIndex];
      const auto &pbrMetallicRoughness = material.pbrMetallicRoughness;

      if (uBaseColorTexture >= 0) {
        if (pbrMetallicRoughness.baseColorTexture.index >= 0) {
          const auto &texture =
              model.textures[pbrMetallicRoughness.baseColorTexture.index];
          if (texture.source >= 0) {
            baseColorTextureObject = textureObjects[texture.source];
          }
          baseColorFactor = pbrMetallicRoughness.baseColorFactor;
        }
      }
      if (uMetallicRoughnessTexture >= 0) {
        if (pbrMetallicRoughness.metallicRoughnessTexture.index >= 0) {
          const auto &texture =
              model.textures[pbrMetallicRoughness.metallicRoughnessTexture
                                 .index];
          if (texture.source >= 0) {
            metallicRoughnessTextureObject = textureObjects[texture.source];
          }
          glUniform1f(uMetallicFactor, pbrMetallicRoughness.metallicFactor);
          glUniform1f(uRougnessFactor, pbrMetallicRoughness.roughnessFactor);
        } else {
          glUniform1f(uMetallicFactor, 1.);
          glUniform1f(uRougnessFactor, 1.);
        }
      }
      if (uEmissiveTexture >= 0) {
        if (material.emissiveTexture.index >= 0) {
          const auto &texture = model.textures[material.emissiveTexture.index];
          if (texture.source >= 0) {
            emissiveTextureObject = textureObjects[texture.source];
          }
        }
      }
      if (uEmissiveFactor >= 0) {
        glUniform3f(uEmissiveFactor, material.emissiveFactor[0],
            material.emissiveFactor[1], material.emissiveFactor[2]);
      }
      if (uOcclusionTexture >= 0) {
        if (material.occlusionTexture.index >= 0) {
          const auto &texture = model.textures[material.occlusionTexture.index];
          if (texture.source >= 0) {
            occlusionTextureObject = textureObjects[texture.source];
          }
        }
      }
      if (uOcclusionStrength >= 0) {
        glUniform1f(uOcclusionStrength, material.occlusionTexture.strength);
      }
      if (uNormalMap >= 0) {
        if (material.normalTexture.index >= 0) {
          const auto &texture = model.textures[material.normalTexture.index];
          if (texture.source >= 0) {
            normalMapTextureObject = textureObjects[texture.source];
          }
        }
      }

      glActiveTexture(GL_TEXTURE0);
      glBindTexture(GL_TEXTURE_2D, baseColorTextureObject);
      glUniform1i(uBaseColorTexture, 0);
      glActiveTexture(GL_TEXTURE1);
      glBindTexture(GL_TEXTURE_2D, metallicRoughnessTextureObject);
      glUniform1i(uMetallicRoughnessTexture, 1);
      glActiveTexture(GL_TEXTURE2);
      glBindTexture(GL_TEXTURE_2D, emissiveTextureObject);
      glUniform1i(uEmissiveTexture, 2);
      glActiveTexture(GL_TEXTURE3);
      glBindTexture(GL_TEXTURE_2D, occlusionTextureObject);
      glUniform1i(uOcclusionTexture, 3);
      glActiveTexture(GL_TEXTURE4);
      glBindTexture(GL_TEXTURE_2D, normalMapTextureObject);
      glUniform1i(uNormalMap, 4);
      glActiveTexture(GL_TEXTURE5);
      glBindTexture(GL_TEXTURE_2D, depthMap);
      glUniform1i(uDepthMapTextureLocation, 5);
      glUniform4f(uBaseColorFactor, baseColorFactor[0], baseColorFactor[1],
          baseColorFactor[2], baseColorFactor[3]);
    }
  };

  const auto drawTexture = [&]() {
    glBindVertexArray(quadVAO);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    glBindVertexArray(0);
  };

  static const auto computeDirectionVectorUp = [](float phiRadians,
                                                   float thetaRadians) {
    const auto cosPhi = glm::cos(phiRadians);
    const auto sinPhi = glm::sin(phiRadians);
    const auto cosTheta = glm::cos(thetaRadians);
    return -glm::normalize(glm::vec3(
        sinPhi * cosTheta, -glm::sin(thetaRadians), cosPhi * cosTheta));
  };

  // Lambda function to draw the scene
  const auto drawScene = [&](const Camera &camera) {
    const auto viewMatrix = camera.getViewMatrix();
    glm::mat4 lightView;
    if (lightFromCamera) {
      lightView = viewMatrix;
    } else {
      const auto dirLightUpVector = computeDirectionVectorUp(phi, theta);
      lightView = glm::lookAt(sceneCenter + lightDirection * sceneRadius,
          sceneCenter, dirLightUpVector);
    }

    // The recursive function that should draw a node
    // We use a std::function because a simple lambda cannot be recursive
    const std::function<void(bool, bool, int, const glm::mat4 &)> drawNode =
        [&](bool drawDepthMap, bool bindMat, int nodeIdx,
            const glm::mat4 &parentMatrix) {
          const tinygltf::Node &node = model.nodes[nodeIdx];
          glm::mat4 modelMatrix = getLocalToWorldMatrix(node, parentMatrix);
          if (node.mesh >= 0) {

            glm::mat4 modelViewMatrix = viewMatrix * modelMatrix;

            glm::mat4 ligthSpaceModelMatrix =
                lightProjMatrix * lightView * modelMatrix;
            if (!drawDepthMap) {
              glm::mat4 modelViewProjectionMatrix =
                  projMatrix * modelViewMatrix;
              glm::mat4 normalMatrix =
                  glm::transpose(glm::inverse(modelViewMatrix));

              glUniformMatrix4fv(modelViewProjMatrixLocation, 1, GL_FALSE,
                  glm::value_ptr(modelViewProjectionMatrix));
              glUniformMatrix4fv(modelViewMatrixLocationRendering, 1, GL_FALSE,
                  glm::value_ptr(modelViewMatrix));
              glUniformMatrix4fv(normalMatrixLocation, 1, GL_FALSE,
                  glm::value_ptr(normalMatrix));
              if (uLightSpaceModelMatrixLocation >= 0) {
                glUniformMatrix4fv(uLightSpaceModelMatrixLocation, 1, GL_FALSE,
                    glm::value_ptr(ligthSpaceModelMatrix));
              }
            } else {
              if (uLightSpaceModelMatrixDepthLocation >= 0) {
                glUniformMatrix4fv(uLightSpaceModelMatrixDepthLocation, 1,
                    GL_FALSE, glm::value_ptr(ligthSpaceModelMatrix));
              }
            }

            tinygltf::Mesh mesh = model.meshes[node.mesh];
            VaoRange vaoRange = meshIndexToVaoRange[node.mesh];

            for (size_t i = 0; i < mesh.primitives.size(); i++) {
              const GLuint vao = vertexArrayObjects[vaoRange.begin + i];
              const tinygltf::Primitive &primitive = mesh.primitives[i];

              if (bindMat) {
                bindMaterial(primitive.material);
              }

              glBindVertexArray(vao);
              if (primitive.indices >= 0) {
                const auto &accessor = model.accessors[primitive.indices];
                const auto &bufferView = model.bufferViews[accessor.bufferView];
                const auto byteOffset =
                    accessor.byteOffset + bufferView.byteOffset;
                glDrawElements(primitive.mode, GLsizei(accessor.count),
                    accessor.componentType, (const GLvoid *)byteOffset);
              } else {
                const auto accessorIdx = (*begin(primitive.attributes)).second;
                const auto &accessor = model.accessors[accessorIdx];
                glDrawArrays(primitive.mode, 0, GLsizei(accessor.count));
              }
            }
          }
          for (const auto childNodeIdx : node.children) {
            drawNode(drawDepthMap, bindMat, childNodeIdx, modelMatrix);
          }
        };

    if ((lightMoved || lightFromCamera) && shadowMappingOn) {

      glslDepthProgram.use();

      glBindFramebuffer(GL_FRAMEBUFFER, depthMapFBO);
      glViewport(0, 0, SHADOW_WIDTH, SHADOW_HEIGHT);
      glClear(GL_DEPTH_BUFFER_BIT);

      // Draw the scene referenced by gltf file
      if (model.defaultScene >= 0) {
        for (const auto nodeIdx : model.scenes[model.defaultScene].nodes) {
          drawNode(true, false, nodeIdx, glm::mat4(1));
        }
      }

      glBindFramebuffer(GL_FRAMEBUFFER, 0);
      lightMoved = false;
    }

    glViewport(0, 0, m_nWindowWidth, m_nWindowHeight);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    if (debugDepth) {
      glslDebugDepthProgram.use();

      if (uDepthMapTextureLocationDebug >= 0) {
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, depthMap);
        glUniform1i(uDepthMapTextureLocationDebug, 0);
      }
      drawTexture();
    } else {
      glslRenderingProgram.use();
      if (uDepthMapTextureLocation >= 0) {
      }
      if (uLightDirectionLocation >= 0) {
        if (lightFromCamera) {
          glUniform3f(uLightDirectionLocation, 0, 0, 1);
        } else {
          glm::vec3 viewSpaceLightDirection = glm::normalize(
              glm::vec3(viewMatrix * glm::vec4(lightDirection, 0.)));

          glUniform3f(uLightDirectionLocation, viewSpaceLightDirection.x,
              viewSpaceLightDirection.y, viewSpaceLightDirection.z);
        }
      }
      if (uLightIntensityLocation >= 0) {
        glUniform3f(uLightIntensityLocation, lightIntensity.x, lightIntensity.y,
            lightIntensity.z);
      }
      if (uOcclusionOn >= 0) {
        glUniform1i(uOcclusionOn, occlusionOn);
      }
      if (normalMapOn >= 0) {
        glUniform1i(uNormalMapOn, normalMapOn);
      }
      if (shadowMappingOn >= 0) {
        glUniform1i(uShadowMapOn, shadowMappingOn);
      }
      // Draw the scene referenced by gltf file
      if (model.defaultScene >= 0) {
        for (const auto nodeIdx : model.scenes[model.defaultScene].nodes) {
          drawNode(0, true, nodeIdx, glm::mat4(1));
        }
      }
    }
  };

  // Setup OpenGL state for rendering
  glEnable(GL_DEPTH_TEST);

  if (!m_OutputPath.empty()) {
    std::vector<unsigned char> pixels(m_nWindowWidth * m_nWindowHeight * 3);
    renderToImage(m_nWindowWidth, m_nWindowHeight, 3, pixels.data(),
        [&]() { drawScene(cameraController->getCamera()); });
    flipImageYAxis(m_nWindowWidth, m_nWindowHeight, 3, pixels.data());
    const auto strPath = m_OutputPath.string();
    stbi_write_png(
        strPath.c_str(), m_nWindowWidth, m_nWindowHeight, 3, pixels.data(), 0);
    return 0;
  }

  // Loop until the user closes the window
  for (auto iterationCount = 0u; !m_GLFWHandle.shouldClose();
       ++iterationCount) {
    const auto seconds = glfwGetTime();

    const auto camera = cameraController->getCamera();
    drawScene(camera);

    // GUI code:
    imguiNewFrame();

    {
      ImGui::Begin("GUI");
      ImGui::Text("Application average %.3f ms/frame (%.1f FPS)",
          1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
      if (ImGui::CollapsingHeader("Camera", ImGuiTreeNodeFlags_DefaultOpen)) {
        ImGui::Text("eye: %.3f %.3f %.3f", camera.eye().x, camera.eye().y,
            camera.eye().z);
        ImGui::Text("center: %.3f %.3f %.3f", camera.center().x,
            camera.center().y, camera.center().z);
        ImGui::Text(
            "up: %.3f %.3f %.3f", camera.up().x, camera.up().y, camera.up().z);

        ImGui::Text("front: %.3f %.3f %.3f", camera.front().x, camera.front().y,
            camera.front().z);
        ImGui::Text("left: %.3f %.3f %.3f", camera.left().x, camera.left().y,
            camera.left().z);

        if (ImGui::Button("CLI camera args to clipboard")) {
          std::stringstream ss;
          ss << "--lookat " << camera.eye().x << "," << camera.eye().y << ","
             << camera.eye().z << "," << camera.center().x << ","
             << camera.center().y << "," << camera.center().z << ","
             << camera.up().x << "," << camera.up().y << "," << camera.up().z;
          const auto str = ss.str();
          glfwSetClipboardString(m_GLFWHandle.window(), str.c_str());
        }
        static int cameraType = 0;
        bool cameraTypeChanged =
            ImGui::RadioButton("Trackball", &cameraType, 0) ||
            ImGui::RadioButton("First Person", &cameraType, 1);
        if (cameraTypeChanged) {
          Camera currentCamera = cameraController->getCamera();
          if (cameraType) {
            cameraController = std::make_unique<FirstPersonCameraController>(
                m_GLFWHandle.window(), 0.5f * maxDistance);
          } else {
            cameraController = std::make_unique<TrackballCameraController>(
                m_GLFWHandle.window(), 0.5f * maxDistance);
          }
          cameraController->setCamera(currentCamera);
        }
      }
      if (ImGui::CollapsingHeader(
              "Shadow Mapping", ImGuiTreeNodeFlags_DefaultOpen)) {
        ImGui::Checkbox("Debug depth map", &debugDepth);
      }
      if (ImGui::CollapsingHeader("Light", ImGuiTreeNodeFlags_DefaultOpen)) {
        ImGui::Checkbox("Light from camera", &lightFromCamera);
        ImGui::Checkbox("Occlusion on", &occlusionOn);
        ImGui::Checkbox("Normal mapping on", &normalMapOn);
        ImGui::Checkbox("Shadow mapping on", &shadowMappingOn);
        if (!lightFromCamera) {
          if (ImGui::SliderFloat("theta", &theta, 0.0f, glm::pi<float>()) ||
              ImGui::SliderFloat("phi", &phi, 0.0f, glm::two_pi<float>())) {
            float sinTheta = glm::sin(theta);
            lightDirection = {sinTheta * glm::cos(phi), glm::cos(theta),
                sinTheta * glm::sin(phi)};
            lightMoved = true;
          }
        }

        static glm::vec3 lightColor(1.f, 1.f, 1.f);
        static float lightIntensityFactor = 1.f;
        if (ImGui::ColorEdit3("color", (float *)&lightColor) ||
            ImGui::InputFloat("intensity", &lightIntensityFactor)) {
          lightIntensity = lightColor * lightIntensityFactor;
        }
        ImGui::Text("light direction: %.3f %.3f %.3f", lightDirection.x,
            lightDirection.y, lightDirection.z);
      }
      ImGui::End();
    }

    imguiRenderFrame();

    glfwPollEvents(); // Poll for and process events

    auto ellapsedTime = glfwGetTime() - seconds;
    auto guiHasFocus =
        ImGui::GetIO().WantCaptureMouse || ImGui::GetIO().WantCaptureKeyboard;
    if (!guiHasFocus) {
      cameraController->update(float(ellapsedTime));
    }

    m_GLFWHandle.swapBuffers(); // Swap front and back buffers
  }

  // TODO clean up allocated GL data

  return 0;
}

ViewerApplication::ViewerApplication(const fs::path &appPath, uint32_t width,
    uint32_t height, const fs::path &gltfFile,
    const std::vector<float> &lookatArgs, const std::string &vertexShader,
    const std::string &fragmentShader, const fs::path &output) :
    m_nWindowWidth(width),
    m_nWindowHeight(height),
    m_AppPath{appPath},
    m_AppName{m_AppPath.stem().string()},
    m_ImGuiIniFilename{m_AppName + ".imgui.ini"},
    m_ShadersRootPath{m_AppPath.parent_path() / "shaders"},
    m_gltfFilePath{gltfFile},
    m_OutputPath{output}
{

  if (!lookatArgs.empty()) {
    m_hasUserCamera = true;
    m_userCamera =
        Camera{glm::vec3(lookatArgs[0], lookatArgs[1], lookatArgs[2]),
            glm::vec3(lookatArgs[3], lookatArgs[4], lookatArgs[5]),
            glm::vec3(lookatArgs[6], lookatArgs[7], lookatArgs[8])};
  }

  if (!vertexShader.empty()) {
    m_vertexShader = vertexShader;
  }

  if (!fragmentShader.empty()) {
    m_fragmentShader = fragmentShader;
  }

  ImGui::GetIO().IniFilename =
      m_ImGuiIniFilename.c_str(); // At exit, ImGUI will store its windows
                                  // positions in this file

  glfwSetKeyCallback(m_GLFWHandle.window(), keyCallback);

  printGLVersion();
}
