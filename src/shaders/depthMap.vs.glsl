#version 330

layout(location = 0) in vec3 aPosition;

uniform mat4 uLightSpaceModelMatrix;

void main() { gl_Position = uLightSpaceModelMatrix * vec4(aPosition, 1.0); }