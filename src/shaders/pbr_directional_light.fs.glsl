#version 330

in vec3 vViewSpacePosition;
in vec3 vViewSpaceNormal;
in vec2 vTexCoords;
in vec4 vLightSpacePosition;
in mat3 mTangentSpace;

uniform vec3 uLightDirection;
uniform vec3 uLightIntensity;
uniform vec4 uBaseColorFactor;
uniform float uMetallicFactor;
uniform float uRougnessFactor;
uniform vec3 uEmissiveFactor;
uniform float uOcclusionStrength;
uniform bool uOcclusionOn;
uniform bool uNormalMapOn;
uniform bool uShadowMapOn;

uniform sampler2D uBaseColorTexture;
uniform sampler2D uMetallicRoughnessTexture;
uniform sampler2D uEmissiveTexture;
uniform sampler2D uOcclusionTexture;
uniform sampler2D uNormalMap;
uniform sampler2D uDepthMapTexture;

out vec3 fColor;

// Constants
const float GAMMA = 2.2;
const float INV_GAMMA = 1. / GAMMA;
const float M_PI = 3.141592653589793;
const float M_1_PI = 1.0 / M_PI;

// We need some simple tone mapping functions
// Basic gamma = 2.2 implementation
// Stolen here:
// https://github.com/KhronosGroup/glTF-Sample-Viewer/blob/master/src/shaders/tonemapping.glsl

// linear to sRGB approximation
// see http://chilliant.blogspot.com/2012/08/srgb-approximations-for-hlsl.html
vec3 LINEARtoSRGB(vec3 color) { return pow(color, vec3(INV_GAMMA)); }

// sRGB to linear approximation
// see http://chilliant.blogspot.com/2012/08/srgb-approximations-for-hlsl.html
vec4 SRGBtoLINEAR(vec4 srgbIn)
{
  return vec4(pow(srgbIn.xyz, vec3(GAMMA)), srgbIn.w);
}

float ShadowCalculation()
{
  vec3 projCoords = vLightSpacePosition.xyz / vLightSpacePosition.w;
  projCoords = projCoords * 0.5 + 0.5;
  float closestDepth = texture(uDepthMapTexture, projCoords.xy).r;
  float currentDepth = projCoords.z;
  vec3 normal = normalize(vViewSpaceNormal);
  vec3 lightDir = uLightDirection;
  float bias = max(0.05 * (1.0 - dot(normal, lightDir)), 0.005);

  float shadow = 0.0;
  vec2 texelSize = 1.0 / textureSize(uDepthMapTexture, 0);
  for (int x = -1; x <= 1; ++x) {
    for (int y = -1; y <= 1; ++y) {
      float pcfDepth =
          texture(uDepthMapTexture, projCoords.xy + vec2(x, y) * texelSize).r;
      shadow += currentDepth - bias > pcfDepth ? 1.0 : 0.0;
    }
  }
  shadow /= 9.0;

  if (projCoords.z > 1.0)
    shadow = 0.0;

  return shadow;
}

void main()
{

  vec3 N = normalize(vViewSpaceNormal);
  vec3 L = uLightDirection;
  vec3 V = normalize(-vViewSpacePosition);
  vec3 H = normalize(L + V);

  vec4 baseColorFromTexture =
      SRGBtoLINEAR(texture(uBaseColorTexture, vTexCoords));
  vec4 metallicRoughnessFromTexture =
      texture(uMetallicRoughnessTexture, vTexCoords);
  vec4 emissiveFromTexture =
      SRGBtoLINEAR(texture(uEmissiveTexture, vTexCoords));
  vec4 occlusionFromTexture = texture(uOcclusionTexture, vTexCoords);
  vec4 normalFromTexture = texture(uNormalMap, vTexCoords);

  vec4 baseColor = baseColorFromTexture * uBaseColorFactor;
  vec3 metallic = vec3(metallicRoughnessFromTexture.b * uMetallicFactor);
  float roughness = metallicRoughnessFromTexture.g * uRougnessFactor;
  vec3 emissive = emissiveFromTexture.rgb * uEmissiveFactor;
  vec3 occlusion = occlusionFromTexture.rgb;
  if (uNormalMapOn) {
    vec3 normal = normalize(normalFromTexture.rgb * 2.0 - 1.0);
    N = normalize(mTangentSpace * normal);
  }

  float alpha = roughness * roughness;
  float sqr_alpha = alpha * alpha;
  float VdotH = clamp(dot(V, H), 0., 1.);
  float NdotH = clamp(dot(N, H), 0., 1.);
  float NdotL = clamp(dot(N, L), 0., 1.);
  float NdotV = clamp(dot(N, V), 0., 1.);

  vec3 dielectricSpecular = vec3(0.04);

  vec3 c_diff =
      mix(baseColor.rgb * (1. - dielectricSpecular), vec3(0.), metallic);
  vec3 f_0 = mix(dielectricSpecular, baseColor.rgb, metallic);

  float baseShlickFactor = 1. - VdotH;
  float shlickFactor = baseShlickFactor * baseShlickFactor;
  shlickFactor *= shlickFactor;
  shlickFactor *= baseShlickFactor;

  vec3 F = f_0 + (vec3(1) - f_0) * shlickFactor;
  float D_den = (NdotH * NdotH) * (sqr_alpha - 1.) + 1.;
  float D = sqr_alpha / (M_PI * D_den * D_den);
  float visDenominator =
      NdotL * sqrt(NdotV * NdotV * (1. - sqr_alpha) + sqr_alpha) +
      NdotV * sqrt(NdotL * NdotL * (1. - sqr_alpha) + sqr_alpha);
  float Vis = visDenominator > 0. ? 0.5 / visDenominator : 0.0;

  vec3 f_diffuse = (1 - F) * M_1_PI * c_diff;
  vec3 f_specular = F * Vis * D;

  float shadow = uShadowMapOn ? ShadowCalculation() : 0;

  vec3 color =
      (f_diffuse + f_specular) * (1.0 - shadow) * uLightIntensity * NdotL +
      emissive;
  if (uOcclusionOn) {
    float ao = texture2D(uOcclusionTexture, vTexCoords).r;
    color = mix(color, color * ao, uOcclusionStrength);
  }

  fColor = LINEARtoSRGB(color);
}