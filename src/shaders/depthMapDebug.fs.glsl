#version 330

in vec2 vTexCoords;

uniform sampler2D uDepthMapTexture;

out vec4 FragColor;

void main()
{
  float depthValue = texture(uDepthMapTexture, vTexCoords).r;
  FragColor = vec4(vec3(depthValue), 1.0);
}